//
//  BuildURL.swift
//  Chuck Norris Jokes
//
//  Created by The App Experts on 30/06/2020.
//  Copyright © 2020 The App Experts. All rights reserved.
//

import UIKit



struct URLBuilder {
    
    static func createURL(jokes: Int = 1, explicit: Bool) -> URL? {
        
        var components = URLComponents()
        // Construct url based on type and input data
        components.scheme = "https"
        components.host = "api.icndb.com"
        components.path = "/jokes/random/\(jokes)"
        if !explicit {
            components.queryItems = [URLQueryItem(name: "exclude", value: "[explicit]")]
        }
        
        return components.url
    }
    
    static func createURL(firstName:String, lastName:String, jokes: Int = 1, explicit: Bool) -> URL? {
        
        var components = URLComponents()
        // Construct url based on type and input data
        components.scheme = "https"
        components.host = "api.icndb.com"
        components.path = "/jokes/random/\(jokes)"
        
        components.queryItems = [URLQueryItem(name: "firstName", value: firstName),
                                 URLQueryItem(name: "lastName", value: lastName)]
        if !explicit {
            components.queryItems?.append(URLQueryItem(name: "exclude", value: "[explicit]"))
        }
        
//        components.queryItems = []
        return components.url
    }
    
}
