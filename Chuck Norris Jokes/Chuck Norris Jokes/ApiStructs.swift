//
//  ApiStructs.swift
//  Chuck Norris Jokes
//
//  Created by The App Experts on 29/06/2020.
//  Copyright © 2020 The App Experts. All rights reserved.
//

import UIKit

struct Root: Codable {
    var value: [Value]
}

struct Value: Codable {
    var id: Int
    var joke: String
}
