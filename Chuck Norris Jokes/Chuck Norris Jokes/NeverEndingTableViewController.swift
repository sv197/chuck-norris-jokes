//
//  NeverEndingTableViewController.swift
//  Chuck Norris Jokes
//
//  Created by The App Experts on 01/07/2020.
//  Copyright © 2020 The App Experts. All rights reserved.
//

import UIKit

class NeverEndingTableViewController: UITableViewController {
    
    var explicit: Bool!
    var model: JokeModel!
    var scrollPosition: IndexPath!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        model = JokeModel()
        loadJokes()
        scrollPosition = IndexPath(row: 0, section: 0)
        NotificationCenter.default.addObserver(self, selector: #selector(rotated(_:)), name: UIDevice.orientationDidChangeNotification, object: nil)
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return model.numberOfSections
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.numberOfRowsInSection()
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "JokeCell", for: indexPath)
        
        // Configure the cell...
        
        let item = model.item(at: indexPath)
        cell.textLabel?.text = item?.joke
        
        if indexPath.row == self.model.jokes.count - 1 {
            self.loadJokes()
        }
        
        return cell
    }
}

extension NeverEndingTableViewController{
    
    func loadJokes() {
        
        guard let url = URLBuilder.createURL(jokes: 20, explicit: explicit) else {
            return
        }
        model.getJSONData(url: url) {
            DispatchQueue.main.async {
                
                self.tableView.reloadData()
            }
        }
        
        return
    }
    
}

extension NeverEndingTableViewController {
    
    override func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        scrollPosition = tableView.indexPathsForVisibleRows?.first
    }
    @objc func rotated(_ notification: Notification) {
        tableView.scrollToRow(at: scrollPosition, at: .top, animated: false)
    }
}
