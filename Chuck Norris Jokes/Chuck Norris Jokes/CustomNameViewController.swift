//
//  CustomNameViewController.swift
//  Chuck Norris Jokes
//
//  Created by The App Experts on 30/06/2020.
//  Copyright © 2020 The App Experts. All rights reserved.
//

import UIKit

class CustomNameViewController: UIViewController {
    @IBOutlet var nameTextField: UITextField!

    var explicit: Bool!
    var model: JokeModel!
    var firstName = ""
    var lastName = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        model = JokeModel()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func search(_ sender: UIButton) {
        
        guard let name = nameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) else { return }
        
        if name.contains(" ") {
            let split = name.split(separator: " ")
            firstName = String(split[0])
            lastName = String(split[1])
            
            guard let url = URLBuilder.createURL(firstName: firstName, lastName: lastName, explicit: explicit) else {
                return
            }
            model.getJSONData(url: url) { [weak self] in
            DispatchQueue.main.async {
                guard let jokeItem = self?.model.getFirstJoke() else { return }
                let joke = jokeItem.joke
                let alert = UIAlertController(title: joke, message: nil, preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in

                    self?.model = JokeModel()
                    
                }))

                    self?.present(alert, animated: true)
                }
            }
            
        } else {
            let alert = UIAlertController(title: "Enter first and last name.", message: nil, preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))

            self.present(alert, animated: true)
        }
        
        
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
