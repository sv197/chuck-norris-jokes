//
//  JokeModel.swift
//  Chuck Norris Jokes
//
//  Created by The App Experts on 30/06/2020.
//  Copyright © 2020 The App Experts. All rights reserved.
//

import UIKit

class JokeModel {
    var jokes: [Value] = []
}

extension JokeModel {
    
    func getFirstJoke() -> Value? {
        return jokes.first
    }
    
}

extension JokeModel {
    var numberOfSections: Int {
        return 1 //return sections
    }
    
    func numberOfRowsInSection() -> Int {
        return jokes.count //use section too if needed
    }
    
    func item(at indexPath: IndexPath) -> Value? {
        if indexPath.section < 0 || indexPath.section > numberOfSections {
            return nil
        }
        if indexPath.row < 0 || indexPath.row > numberOfRowsInSection() {
            return nil
        }
        
        return jokes[indexPath.row]
    }
}

extension JokeModel {
    
    func getJSONData(url: URL, completion: @escaping () -> Void) {
        //create session
        let defaultSession = URLSession(configuration: .default)
        //        guard let url = URLBuilder.createURL(term, type: dataType) else { return }
        
        //send request for data
        let dataTask = defaultSession.dataTask(with: url) { (data, response, error) in
            
            if let error = error {
                print("Error - \(error.localizedDescription)")
            } else if let response = response as? HTTPURLResponse {
                
                switch response.statusCode {
                case 200...299:
                    if let data = data {
                        //parse data
                        self.parseJSONData(data)
                    }
                default: print("Found \(response.statusCode)")
                }
            }
            //reload table
            completion()
        }
        //start task
        dataTask.resume()
    }
    
    func parseJSONData(_ data: Data) {
        let decoder = JSONDecoder()
        do {
            //decode data
            
            let decodedData = try decoder.decode(Root.self, from: data)
            //get array from decoded data
            let results = decodedData.value
            //loop through data
            for result in results {
                //add data to model
                self.jokes.append(result)
            }
        } catch {
            print(error)
        }
    }

}
