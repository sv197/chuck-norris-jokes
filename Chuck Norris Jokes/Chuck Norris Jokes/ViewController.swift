//
//  ViewController.swift
//  Chuck Norris Jokes
//
//  Created by The App Experts on 29/06/2020.
//  Copyright © 2020 The App Experts. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet var explicitSwitch: UISwitch!
    var model: JokeModel!
    var explicit:Bool!
    override func viewDidLoad() {
        super.viewDidLoad()
        explicit = true
        model = JokeModel()
    }

    @IBAction func randomJoke(_ sender: UIButton) {
        
        explicit = explicitSwitch.isOn
        guard let url = URLBuilder.createURL(explicit: explicit) else {
            return
        }
        model.getJSONData(url: url) { [weak self] in
        DispatchQueue.main.async {
            guard let jokeItem = self?.model.getFirstJoke() else { return }
            let joke = jokeItem.joke
            let alert = UIAlertController(title: joke, message: nil, preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in

                self?.model = JokeModel()
                
            }))

                self?.present(alert, animated: true)
            }
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let nextViewController = segue.destination as? CustomNameViewController {
            // Pass selected item data
            nextViewController.explicit = explicit
        } else if let nextViewController = segue.destination as? NeverEndingTableViewController {
            // Pass selected item data
            nextViewController.explicit = explicit
        }
    }
    @IBAction func input(_ sender: UIButton) {
        
        explicit = explicitSwitch.isOn
        performSegue(withIdentifier: "BeChuck", sender: self)
    }
    
    @IBAction func neverEndingJokes(_ sender: Any) {
        
        explicit = explicitSwitch.isOn
        performSegue(withIdentifier: "ShowJokes", sender: self)
    }
    
}

