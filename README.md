# How to build the app

1. First, design the UI and view layouts.
1. Next create structs to handle the JSON response from the API server.
1. Then create the URL builder to construct urls for the different types of requests.
1. After that create code to send the requests and decode the response.
1. Finally, display the decoded data as required by the specifications.